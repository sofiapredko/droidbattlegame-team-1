package com.game;

import java.util.Random;
import java.util.Scanner;


public class Medic implements Fighter {


    public String name;
    private int energy = 10;
    private int hp = 30;
    private int maxDamage = 5;
    private int defense = 5;
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    public Medic() {
        System.out.println("\nHi, i am your Medic. Give me a name please.");
        System.out.println(ANSI_YELLOW + medicPicture + ANSI_RESET);
        Scanner sc = new Scanner((System.in));
        this.name = sc.nextLine();
        System.out.println("\nI am a Medic. My name wil be " +
                this.name + ". I fight with syringes and heal myself. " +
                "You can`t beat me");
    }

    public void printCurrentStats() {
        System.out.println(this.getName() + "'s stats are:" +
                "\nEnergy: " + this.getEnergy() + " " +
                "   HP: " + this.getHp() + " " +
                "   Defence: " + this.getDefense());
        System.out.println(ANSI_YELLOW + medicPicture + ANSI_RESET);
    }

    public void receiveDamage(int damage) {
        if (this.getDefense()>=damage){
            this.setDefense(this.getDefense() - damage );
        }else{
            this.setHp(this.getHp() - (damage - this.getDefense()));
            this.setDefense(0);
        }


    }

    public void attack(Fighter target) {
        if (this.getEnergy() > 0) {
            Random r = new Random();
            int causedDamage = (int) (Math.random() * getMaxDamage());
            target.receiveDamage(causedDamage);
            this.setEnergy(this.getEnergy() - 1);
            int heal = causedDamage;
            this.setHp(this.getHp() + heal);
            System.out.println("\nI attack your enemy with " + causedDamage + " damage and heal myself for "+causedDamage+" HP.");
            System.out.println(ANSI_YELLOW + medicAttackPicture + ANSI_RESET);
        } else {
            System.out.println("\nYou are out of energy, you cannot perform any action");
        }
    }

    public void defend() {
        if (this.getEnergy() > 0) {
            Random r = new Random();
            int raisedDef = (int) (Math.random() * 5);
            this.setDefense(this.getDefense() + raisedDef);
            this.setEnergy(this.getEnergy() - 1);
            System.out.println("I Defend. Now my defense is " + this.getDefense());
            System.out.println(ANSI_YELLOW + deff  + ANSI_RESET);
        } else {
            System.out.println("\nYou are out of energy, you cannot perform any action");
        }
    }


    public boolean isAlive() {
        if (getHp() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public void skipAndPray() {
        System.out.println("I refuse to have a move");
    }

    public void giveUp() {
        System.out.println("I give up. You win");
        this.setHp(0);
    }

    public boolean hasEnergy(){
        if(this.getEnergy()>0){
            return true;
        }else{
            return false;
        }
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public String getName() {
        return name;
    }

    public void printCyanText(String cyanText){System.out.println(ANSI_YELLOW + medicPicture + ANSI_RESET);}

    String medicPicture = String.join("\n"
            ,".                          "
            ,"          ^     ^               "
            ,"           ^___^                  "
            ,"           *o o*                  "
            ,"         _*_---_*_              "
            ,"     **** ^^^^^^^ ****           "
            ,"     || &| __|__ |& ||             "
            ,"     ** &|   |   |& **            "
            ,"     ||  *********  ||            "
            ,"     @@     |||     @@              "
            ,"         ___^^^___                     "
            ,"        {000000000}                "
    );

    String medicAttackPicture = String.join("\n"
            ,".                          "
            ,"          ^     ^               "
            ,"           ^___^                  "
            ,"           *o o*                  "
            ,"         _*_---_*_              PUF-PUF  "
            ,"     **** ^^^^^^^ ****--*---|   ______       "
            ,"     || &| __|__ |& ----*---|           "
            ,"     ** &|   |   |&             "
            ,"     ||  *********              "
            ,"     @@     |||                   "
            ,"         ___^^^___                 "
            ,"        {000000000}                "
    );
    String deff = String.join("\n"
            , ".                          "
            , "                "
            , "         &&&&&&                          "
            , "        &      &        "
            , "  &&&&&          &&&&&     "
            , "  & &&&&&&&          &      "
            , "  & &&&&&&&          &     "
            , "  & &&&&&&&          &     "
            , "   &        &&&&&&& &      "
            , "    &       &&&&&& &        "
            , "     &      &&&&& &         "
            , "       &    &&&&&          "
            , "         &&&&&&                 "
            , "                       "
            , "                 "
    );
}
