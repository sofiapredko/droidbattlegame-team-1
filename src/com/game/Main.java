package com.game;

import java.util.InputMismatchException;

public class Main {
    public static void main(String[] args) {
        Fighter userOne;
        Fighter userTwo;
        do {
            while (true){
                try {
                    userOne = Game.chooseFighter("One");
                    break;
                } catch (InputMismatchException e) {
                    System.out.println("Please input integers 1, 2 or 3 ONLY!");
                }
            }
           while (true){
               try {
                   userTwo = Game.chooseFighter("Two");
                   break;
               } catch (InputMismatchException e) {
                   System.out.println("Please input integers 1, 2 or 3 ONLY!");
               }
           }
            Game.newGame(userOne, userTwo);
        } while (Game.askForNew());
        System.out.println("Ok, thank you for playing");

        //  if (Game.askForNew()) {
        //    System.out.println("Here we go again!");
        //   userOne = Game.chooseFighter();
        // userTwo = Game.chooseFighter();
        //Game.newGame(userOne, userTwo);
        //} else {

        //  return;
        //}


        //battleDroidtoEnd(d1, d2);
        //  Scanner scan = new Scanner(System.in);
        /// System.out.println("1 - continue game, play again");
        //boolean game = true;



            /*  test fight two beats
            strike(d1, d2);
            int enemydam = strike(d1, d2);
            defend(d2, enemydam);
            strike(d1, d2);
            strike(d2, d1);
            enemydam = strike(d2, d1);
            defend(d1, enemydam);
            */

    }
}
