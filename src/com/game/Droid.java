package com.game;

import java.util.Random;
import java.util.Scanner;

public class Droid implements Fighter {

    public String name;
    private int energy = 10;
    private int hp = 30;
    private int maxDamage = 10;
    private int defense = 5;
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_RESET = "\u001B[0m";

    public Droid() {
        System.out.println("Hi, i am your Droid. Give me a name please.");
        Scanner sc = new Scanner((System.in));
        System.out.println(ANSI_CYAN + droidPicture + ANSI_RESET);
        this.name = sc.nextLine();
        System.out.println("I am a Droid. My name wil be " +
                this.name + ". I fight Shots. " +
                "You can`t beat me");
    }

    public void printCurrentStats() {
        System.out.println(this.getName() + "'s stats are:" +
                "\nEnergy: " + this.getEnergy() + " " +
                "   HP: " + this.getHp() + " " +
                "   Defence: " + this.getDefense());
        System.out.println(ANSI_CYAN + droidPicture + ANSI_RESET);

    }

    public void receiveDamage(int damage) {
        if (this.getDefense()>=damage){
            this.setDefense(this.getDefense() - damage );
        }else{
            this.setHp(this.getHp() - (damage - this.getDefense()));
            this.setDefense(0);
        }    }

    public void attack(Fighter target) {
        if (this.getEnergy() > 0) {
            Random r = new Random();
            int causedDamage = (int) (Math.random() * getMaxDamage());
            target.receiveDamage(causedDamage);
            this.setEnergy(this.getEnergy() - 1);
            System.out.println("I beat your enemy with " + causedDamage + " damage");
            System.out.println(ANSI_CYAN + droidAttackPicture + ANSI_RESET);
        } else {
            System.out.println("You are out of energy, you cannot perform any action");
        }
    }

    public void defend() {
        if (this.getEnergy() > 0) {
            Random r = new Random();
            int raisedDef = (int) (Math.random() * 5);
            this.setDefense(this.getDefense() + raisedDef);
            this.setEnergy(this.getEnergy() - 1);
            System.out.println("I Defend. Now my defense is " + this.getDefense());
            System.out.println(ANSI_CYAN + deff + ANSI_RESET);
        } else {
            System.out.println("You are out of energy, you cannot perform any action");
        }
    }


    public boolean isAlive() {
        if (getHp() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public void skipAndPray() {
        System.out.println("I refuse to perform an Action");
    }

    public void giveUp() {
        System.out.println("I give up. You win");
        this.setHp(0);
    }

    public boolean hasEnergy(){
        if(this.getEnergy()>0){
            return true;
        }else{
            return false;
        }
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public String getName() {
        return name;
    }

    String droidPicture = String.join("\n"
            ,".                                "
            ,">>>>>>>|<<<<<<<    o    >>>>>>>|<<<<<<<    "
            ,"      _^_       __ | __       _^_          "
            ,"    ([&&&])----||*****||----([&&&])        "
            ,"     *****     | -(O)- |     *****         "
            ,"                 *---*                     "
            ,"               _&_   _&_                   "
    );

    String droidAttackPicture = String.join("\n"
            ,".                                "
            ,">>>>>>>|<<<<<<<    o    >>>>>>>|<<<<<<<    "
            ,"      _^_       __ | __       _^_          "
            ,"    ([&&&])----||*****||----([&&&])        "
            ,"     *****     | -(O)- |     *****         "
            ,"      (]---      *---*        (]---  PIU-PIU-PIU "
            ,"               _&_   _&_                  "
            ,"                                           "
    );

    String deff = String.join("\n"
            , ".                          "
            , "                "
            , "         &&&&&&                          "
            , "        &      &        "
            , "  &&&&&          &&&&&     "
            , "  & &&&&&&&          &      "
            , "  & &&&&&&&          &     "
            , "  & &&&&&&&          &     "
            , "   &        &&&&&&& &      "
            , "    &       &&&&&& &        "
            , "     &      &&&&& &         "
            , "       &    &&&&&          "
            , "         &&&&&&                 "
            , "                       "
            , "                 "
    );




}
