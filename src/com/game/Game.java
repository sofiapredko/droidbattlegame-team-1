package com.game;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_RED = "\u001B[31m";

	public static Fighter chooseFighter(String user) {
		System.out.println("\nHello, anon#" + user + "\nWelcome to the most stupid game youll ever see." +
				"\nPick up your fighter:\n" +
				"1 - Droid (HP = 30, energy = 10; defense = 5, Max.damage = 10)\n" +
				"2 - Robot (HP = 30, energy = 10; defense = 10, Max.damage = 8)\n" +
				"3 - Medic (HP = 30, energy = 10; defense = 5, Max.damage = 5. Has special skill - 'Heal')\n");
		Scanner in = new Scanner(System.in);
		int choice = in.nextInt();
		if (choice == 1) {
			Fighter droid = new Droid();
			return droid;
		} else if (choice == 3) {
			Medic medic = new Medic();
			return medic;
		} else if (choice == 2) {
			Robot robot = new Robot();
			return robot;
		}
		return null;
	}


	public static void newGame(Fighter userOne, Fighter userTwo) {
		while (userOne.isAlive() && userTwo.isAlive()) {
			while ((userOne.hasEnergy()) && (userTwo.hasEnergy())) {
				while (true) {
					try {
						Game.askForAction(userOne, userTwo);
						break;
					} catch (InputMismatchException e) {
						System.out.println("\nPlease input integers 1, 2 , 3 or 4 ONLY!\n");
					}
				}

				if (userTwo.isAlive()) {
					while (true) {
						try {
							Game.askForAction(userTwo, userOne);
							break;
						} catch (InputMismatchException e) {
							System.out.println("\nPlease input integers 1, 2 , 3 or 4 ONLY!\n");
						}
					}
				} else {
					gameEnd(userOne);
					return;
				}
			}
			gameEnd(findWinner(userOne, userTwo));
			break;
		}
	}


	public static void askForAction(Fighter fighter, Fighter target) throws InputMismatchException {
		if (Game.gameOn(fighter, target)) {
			System.out.println("_________________________________");
			System.out.println("\nNow is the " + fighter.getName() + "'s turn.\n");
			System.out.println("---------------------------------");
			fighter.printCurrentStats();
			System.out.println("\nWhat do you want to do?" +
					"\n1 - Attack " +
					"  2 - Defend " +
					"  3 - SkipMove " +
					"  4 - Refuse to fight");
			Scanner sc = new Scanner(System.in);
			switch (sc.nextInt()) {
				case 1: {
					fighter.attack(target);
					break;
				}
				case 2: {
					fighter.defend();
					break;
				}
				case 3: {
					fighter.skipAndPray();
					break;
				}
				case 4: {
					fighter.giveUp();
					break;
				}
				default: {
					throw new InputMismatchException();
				}

			}
		}
	}


	public static Fighter findWinner(Fighter userOne, Fighter userTwo) {

		if ((userOne.getEnergy() == 0) && (userTwo.getEnergy() == 0)) {

			if (userOne.getHp() > userTwo.getHp()) {
				return userOne;
			} else if (userOne.getHp() < userTwo.getHp()) {
				return userTwo;
			} else {
				return null;
			}

		}


		if (!userOne.isAlive()) {
			return userTwo;
		} else {
			return userOne;
		}

	}

	public static void gameEnd(Fighter winner) {
		if (winner != null) {
			System.out.println("\nThe Game is over." +
					"\nFighter " + winner.getName() + " is a Winner");
		} else {
			System.out.println("\nThe Victory goes to the Friendship.\n" +
					"Remember, Friendship is magic!\n");
		}
		printGameEnd();
	}

	public static boolean askForNew() {
		System.out.println("Do u wanna play again? + Yes, anything else - No");
		Scanner sc = new Scanner(System.in);
		if (sc.nextLine().equals("+")) {
			return true;
		} else {
			return false;
		}
	}


	public static boolean gameOn(Fighter fighterOne, Fighter fighterTwo) {
		if (fighterOne.isAlive() && fighterTwo.isAlive()) {
			return true;
		} else {
			return false;
		}

	}

	public static void printGameEnd() {
		String gameOver = String.join("\n"
				, ".                                  "
				, "    * ** *         *        *         *  * * * *      "
				, "   *      *       * *       * *     * *  *             "
				, "  *              *   *      *  *   *  *  *           "
				, "  *    ***      *     *     *   * *   *  ** **        "
				, "  *       *    * * * * *    *    *    *  *            "
				, "   *      *   *         *   *         *  *            "
				, "    * ** *   *           *  *         *  * * * *      "
				, "                              "
				, "      * ** *   *           *  * * * *   * ** *                              "
				, "     *      *   *         *   *         *      *           "
				, "    *        *   *       *    *         *      *     "
				, "    *        *    *     *     ** **     * * * *         "
				, "    *        *     *   *      *         *    *       "
				, "     *      *       * *       *         *     *      "
				, "      * ** *         *        * * * *   *      *     "
				, "                           "
		);
		System.out.println(ANSI_RED + gameOver + ANSI_RESET);
	}
}


