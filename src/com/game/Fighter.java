package com.game;

public interface Fighter {

    boolean hasEnergy();

    void printCurrentStats();

    void receiveDamage(int damage);

    int getEnergy();

    int getHp();

    String getName();

    void defend();

    void attack(Fighter target);

    boolean isAlive();

    void skipAndPray();

    void giveUp();
}
