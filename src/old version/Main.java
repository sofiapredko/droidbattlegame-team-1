import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Droid d1 = new Droid(5, 50, 50);
        Droid d2 = new Droid(5, 50, 50);
        Game game = new Game();

        Scanner scan = new Scanner(System.in);
        System.out.println("1 - continue game, play again");
        //boolean game = true;

        while(game.gameOn(d1 ,d2)) {

            System.out.println("1 - attack, 2 - defend, 3 - nothing");
            System.out.println();
            System.out.println("Player one - choose");
            int choice1 = scan.nextInt();

            switch (choice1) {
                case 1:
                    game.strike(d1, d2);
                    break;
                case 2:
                    int enemyDamage = game.strike(d2, d1);
                    game.defend(d1, enemyDamage);
                    break;
               /* case 3:
                    d1.skipAndPray();
                    break;  */

                default:
                    System.out.println("--- Player 1 Info ---");
                    d1.getPlayerStatus();
                    game.isWinner(d1, d2);
            }

            System.out.println();
            System.out.println("Player two - choose");
            int choice2 = scan.nextInt();
            switch (choice2) {
                case (1):
                    game.strike(d2, d1);
                    break;
                case (2):
                    int enemydam = game.strike(d1, d2);
                    game.defend(d2, enemydam);
                case (3):
                    d2.skipAndPray();
                default:
                    System.out.println("--- Player 2 Stats ---");
                    d2.getPlayerStatus();
                    game.isWinner(d1, d2);
            }

                System.out.println("Next Round");
        }

        game.isWinner(d1, d2);

    }
}
